//
//  TrackManager.swift
//  muzisTestApp
//
//  Created by vladimir on 17.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import Foundation


class Track {
    
    var id: Int!
    var albumCover: URL!
    var trackURL: URL!
    var artistName: String!
    var trackName: String!
    
    init (_ id: Int, _ albumCover: URL, _ trackURL: URL, _ artistName: String,  _ trackName: String) {
        self.id = id
        self.albumCover = albumCover
        self.trackURL = trackURL
        self.artistName = artistName
        self.trackName = trackName
    }
    
    convenience init(_ id: Int, _ trackURL: URL, _ trackName: String) {
        
        let artistName = "Artist"
        let albumCover = URL(string: "https://pp.userapi.com/c837229/v837229437/3eb96/FNAFucr-wcY.jpg")
        self.init(id, albumCover!, trackURL, artistName, trackName )
    }
}

class PlaylistManager {
    
    var tracks = [Track]()
    
    func setupPlaylist(_ tracks: [Track]) {
        
    }
    
    func mockPlaylist() {
        tracks.append(Track(1 , URL(string: "http://www.billboard.com/files/styles/900_wide/public/media/Joy-Division-Unknown-Pleasures-album-covers-billboard-1000x1000.jpg")!, Bundle.main.url(forResource: "Love Will Tear Us Apart", withExtension: "mp3")!, "Joy Division", "Love Will Tear Us Apart"))
        tracks.append(Track(1 , URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRg-SknkebQldf8jWObFhcZk9VCQheMqBTQ1V3q4aKoUMSLKC1x")!, Bundle.main.url(forResource: "Goodbye Cruel World", withExtension: "mp3")!, "Pink Floyd", "Goodbye Cruel World"))
        tracks.append(Track(1 , URL(string: "https://s3-eu-central-1.amazonaws.com/centaur-wp/designweek/prod/content/uploads/2016/01/11163836/aladdin-sane-album-cover-david-bowie.jpg")!,  Bundle.main.url(forResource: "Heroes", withExtension: "mp3")!, "David Bowie", "Heroes"))
        tracks.append(Track(1 , URL(string: "https://upload.wikimedia.org/wikipedia/en/9/96/Adele_-_25_%28Official_Album_Cover%29.png")!, Bundle.main.url(forResource: "Hello", withExtension: "mp3")!, "Adele", "Hello"))
        
        
    }
    
}
