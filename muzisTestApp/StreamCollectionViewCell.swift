//
//  StreamCollectionViewCell.swift
//  muzisTestApp
//
//  Created by vladimir on 16.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit

class StreamCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var streamTitleLabel: UILabel!
    @IBOutlet weak var streamPlayButton: UIButton!
    @IBOutlet weak var streamCoverImage: UIImageView!
    
}
