//
//  PlayListTableViewCell.swift
//  muzisTestApp
//
//  Created by vladimir on 18.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit

class PlayListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.clear
        
        self.albumCover.layer.cornerRadius = self.albumCover.frame.size.width / 2;
        self.albumCover.clipsToBounds = true
        
    }
}
