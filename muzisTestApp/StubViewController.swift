//
//  StubViewController.swift
//  muzisTestApp
//
//  Created by vladimir on 17.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit

class StubViewController: CanContainMiniPlayerViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Радио для двоих"
        let infoLabel = UILabel(frame: CGRect(x: self.view.center.x - 75, y: self.view.center.y, width: 150, height: 20))
        infoLabel.text = "To be implemented"
        self.view.addSubview(infoLabel)
    }
}
