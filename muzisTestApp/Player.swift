//
//  Player.swift
//  muzisTestApp
//
//  Created by vladimir on 16.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import AVFoundation


class Player {
    
    let player: AVPlayer!
    
    init() {
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            print("error")
        }
        
        player = AVPlayer()
    }
    
    func replace(track: URL) {
        let item: AVPlayerItem = AVPlayerItem(url: track)
        player.replaceCurrentItem(with: item)
    }
    
    func play() {
    
        player.play()
    }
    
    func pause() {
        player.pause()
    }
}
