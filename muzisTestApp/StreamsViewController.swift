//
//  StreamsViewController.swift
//  muzisTestApp
//
//  Created by vladimir on 16.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit
import SDWebImage


class StreamsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var streams = [Stream]()
    var streamManager = StreamManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let tabBarController = self.tabBarController as? ContentTabBarController else { return }
        
        tabBarController.miniPlayer.isHidden = true
    
        setupStreams()
        collectionView.dataSource = self
        collectionView.delegate = self
        presentAlert()
        
    }
    
    func setupStreams() {
        self.streams = streamManager.mockStreams()
        self.collectionView.reloadData()
    }
    
    func presentAlert() {
        
        let alert = UIAlertController(title: "", message: "Войдите удобным для Вас способом", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default, handler: nil) )
        alert.addAction(UIAlertAction(title: "VK", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Электронная почта", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Мобильный телефон", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Отменить", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - UICollectionViewDelegate
extension StreamsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = streams[indexPath.item]
        PlayerViewController.instance.navigationItem.title = data.title
        navigationController?.pushViewController(PlayerViewController.instance, animated: true)
    }
}

// MARK: - UICollectionViewDataSource
extension StreamsViewController: UICollectionViewDataSource {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return streams.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "streamCell", for: indexPath) as! StreamCollectionViewCell
        
        let data = streams[indexPath.item];
        
        cell.streamCoverImage.sd_setImage(with: data.coverURL)
        cell.streamTitleLabel.text = data.title
        
        return cell
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension StreamsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        

        let padding: CGFloat = 8
        let cellSize = (collectionView.frame.size.width - padding) / 2
        
        return CGSize(width: cellSize, height: cellSize)
        
    }
}

