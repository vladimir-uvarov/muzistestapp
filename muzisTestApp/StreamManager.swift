//
//  StreamManager.swift
//  muzisTestApp
//
//  Created by vladimir on 17.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import Foundation
import UIKit

class Stream {
    var url:URL!
    var coverURL: URL!
    var title: String!
    
    init( _ title: String, _ coverURL: URL) {
        self.title = title
        self.coverURL = coverURL
    }
}

class StreamManager {
    
    func setupStreams(_ stream: Stream) {
        
    }
    
    func mockStreams() -> [Stream] {
        
        var streams = [Stream]()
        
        streams.append(Stream("Latin", URL(string: "https://s-media-cache-ak0.pinimg.com/236x/e5/37/6d/e5376d642c2afff021f41c406178b489.jpg")!))
        streams.append(Stream("Rock", URL(string: "http://ep.yimg.com/ay/gallerydirectart/the-rolling-stones-rock-album-cover-art-on-stretched-archival-canvas-tougne-2.jpg")!))
        streams.append(Stream("Jazz", URL(string: "http://www.birkajazz.com/graphics2/rollinsColossus.jpg")!))
        streams.append(Stream("Hip Hop", URL(string: "http://hiphop-n-more.com/wp-content/uploads/2014/01/mastermind-deluxe.jpg")!))
        streams.append(Stream("Vaporwave", URL(string: "https://upload.wikimedia.org/wikipedia/en/e/e9/Floral_Shoppe_Alt_Cover.jpg")!))
        streams.append(Stream("Classical", URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnfGx-jv9xfKgmFdiKqS51EhUZ8Y6ya0puiJbGA_36ry8er4K9vYHG9Q")!))

        return streams
        
    }
    
}
