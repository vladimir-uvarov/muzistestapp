//
//  contentTabBarController.swift
//  muzisTestApp
//
//  Created by vladimir on 17.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit

class ContentTabBarController: UITabBarController {// , PlayerHandlerDelegate {
    
    var miniPlayer = MinimizePlayerView()
    var initFrame: CGRect!
    var notifier = PlayerViewHandler()

	
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(miniPlayer)
        miniPlayer.pauseButton.addTarget(self, action: #selector(ContentTabBarController.pausePlayer), for: .touchUpInside)
        
        miniPlayer.isHidden = false
        
        initFrame = self.view.bounds
			
        NotificationCenter.default.addObserver(forName: PlayerManager.trackBeenChanged,
			                                       object: nil,
			                                       queue: nil) { [weak self] _ in
        guard let `self` = self,
            let currentTrack = PlayerManager.shared.currentTrack else { return }
            self.changeMiniplayer(track: currentTrack)
        }
    }
	
	private func changeMiniplayer(track: Track) {
		self.miniPlayer.artistNameLabel.text = track.artistName
        self.miniPlayer.trackNameLabel.text = track.trackName
        
	}
    
    func pausePlayer() {
        PlayerViewController.instance.setStatus(.pause)
        PlayerViewController.instance.player.pause()
        hidePlayer()
        notifier.notify()
    }
    
    func hidePlayer() {
        miniPlayer.isHidden = true
    }
    
    func showPlayer() {
        miniPlayer.isHidden = false
    }
    
    func trackDidChange(_ track: Track) {
        
        if let instance = self.tabBarController as? ContentTabBarController {
            instance.miniPlayer.trackNameLabel.text = track.trackName
        
        }
    }

}
// MARK: - UITabBarControllerDelegate

extension ContentTabBarController: UITabBarControllerDelegate {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if self.tabBar.items?.index(of: item) == 0 {
            hidePlayer()
        } else {
            if PlayerViewController.instance.status == .play {
                showPlayer()
            }
            
        }
    }
}


