//
//  PlayerManager.swift
//  muzisTestApp
//
//  Copyright © 2017 vladimir. All rights reserved.
//

import Foundation

class PlayerManager {
	
	static let shared = PlayerManager()
	static let trackBeenChanged = Notification.Name("com.muzistest.playermanager.trackbeenchanged")
	
	var currentTrack: Track?
	
	public func changeTrack(_ track: Track) {
		self.currentTrack = track
		NotificationCenter.default.post(name: PlayerManager.trackBeenChanged, object: nil)
	}
	
}
