//
//  CanContainMiniPlayerViewController.swift
//  muzisTestApp
//
//  Created by vladimir on 18.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit

class CanContainMiniPlayerViewController: UIViewController {
    
    var originalFrame: CGRect!
    var playerActiveFrame: CGRect!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        originalFrame = self.view.bounds
        playerActiveFrame = CGRect(x: 0.0, y: 50.0, width: originalFrame.size.width, height: originalFrame.height - 50.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.view.setNeedsLayout()
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if PlayerViewController.instance.status == .play {
            self.view.frame = playerActiveFrame
        } else {
            self.view.frame = originalFrame
        }
        
    }
}

// MARK: - PlayerHandlerDelegate

extension CanContainMiniPlayerViewController: PlayerHandlerDelegate {
    func miniPlayerDidClose() {
        
        if let topController = UIApplication.topViewController() as? CanContainMiniPlayerViewController {
            topController.view.frame = self.view.bounds
        }
    }
    
}

