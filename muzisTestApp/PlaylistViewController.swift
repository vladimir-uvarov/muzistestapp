//
//  PlaylistViewController.swift
//  muzisTestApp
//
//  Created by vladimir on 17.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit
import SDWebImage

class PlaylistViewController: CanContainMiniPlayerViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var playlist = [Track]()
    var playlistManager = PlaylistManager()
    var playerHandler = PlayerHandler()
    
    @IBOutlet weak var currentTrackBackground: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        setupPlaylist()
        
    }
    
    func setupPlaylist() {
        playlistManager.mockPlaylist()
        self.playlist = playlistManager.tracks
        tableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if PlayerViewController.instance.status == .play{
            guard let tabBarController = self.tabBarController as? ContentTabBarController else { return }
					tabBarController.miniPlayer.isHidden = false
        }
        
        currentTrackBackground.image = PlayerViewController.instance.backgroundImage.image
    }

}

// MARK: - UITableViewDataSource

extension PlaylistViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayListCell", for: indexPath) as! PlayListTableViewCell
        
        let track = playlist[indexPath.row]
        
        cell.artistNameLabel.text = track.artistName
        cell.trackNameLabel.text = track.trackName
        
        cell.albumCover.sd_setImage(with: track.albumCover)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension PlaylistViewController: UITableViewDelegate {
	
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
			PlayerManager.shared.changeTrack(playlist[indexPath.row])
            playerHandler.changeTrack(playlist[indexPath.row])
            currentTrackBackground.sd_setImage(with: playlist[indexPath.row].albumCover)
    }

}
