//
//  PlayerViewController.swift
//  muzisTestApp
//
//  Created by vladimir on 16.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit


enum PlayerStatus {
    
    case play, pause
    
}

class PlayerViewController: UIViewController , PlayerDelegate {
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var currentTrackCover: UIImageView!
    @IBOutlet weak var currentTrackLabel: UILabel!
    @IBOutlet weak var currentTrackArtist: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    static var instance: PlayerViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        return vc
    }()
    
    let player = Player()
    var status: PlayerStatus = .pause
    var currentTrack: Track?
   
    override func viewDidLoad() {
        
        playButton.addTarget(self, action:  #selector(PlayerViewController.startPlayback), for: .touchUpInside)
        moreButton.addTarget(self, action:  #selector(PlayerViewController.openPlaylist), for: .touchUpInside)
        backgroundImage.contentMode = .scaleAspectFill
        currentTrackCover.layer.borderWidth = 2
        currentTrackCover.layer.cornerRadius = 5
        currentTrackCover.layer.borderColor = UIColor.white.cgColor
        
    }
    
    func setStatus(_ status: PlayerStatus) {
        self.status = status
    }
    
    override func viewWillAppear(_ animated: Bool) {
			super.viewWillAppear(animated)

			guard let tabBarController = self.tabBarController as? ContentTabBarController else { return }
			tabBarController.miniPlayer.isHidden = true
        
            if status == .play {
                playButton.setImage(#imageLiteral(resourceName: "pause") , for: .normal)
            } else {
                playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            }
    }
    
    func startPlayback() {
        if status == .pause {
            setStatus(.play)
            playButton.setImage(#imageLiteral(resourceName: "pause") , for: .normal)
            player.play()
            
        } else {
            status = .play
            setStatus(.pause)
            playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            player.pause()
        }
    }
    
    func openPlaylist() {
        let playlistVC = storyboard?.instantiateViewController(withIdentifier: "PlaylistViewController") as! PlaylistViewController
        navigationController?.pushViewController(playlistVC, animated: true)
    }
    
    func trackDidChange( _ track: Track) {
        self.currentTrack = track
        currentTrackLabel.text = track.trackName
        currentTrackCover.sd_setImage(with: track.albumCover)
        currentTrackArtist.text = track.artistName
        self.backgroundImage.sd_setImage(with: track.albumCover)
        player.replace(track: track.trackURL)
    }
}
