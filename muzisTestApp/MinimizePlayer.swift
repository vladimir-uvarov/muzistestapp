//
//  MinimizePlayer.swift
//  muzisTestApp
//
//  Created by vladimir on 17.05.17.
//  Copyright © 2017 vladimir. All rights reserved.
//

import UIKit

class MinimizePlayerView: UIView {
	
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var view: UIView!
    
    convenience init() {
        let screenSize = UIScreen.main.bounds
        let frame = CGRect(x: 0, y: 64, width: screenSize.width, height: 50)
        self.init(frame: frame)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "MinimizePlayer", bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
}


protocol PlayerHandlerDelegate  {
    func miniPlayerDidClose()
}


protocol  PlayerDelegate  {
    
    func trackDidChange(_ track: Track)
}

class PlayerHandler {
    
    var delegate: PlayerDelegate?
    
    func changeTrack(_ track: Track) {
        delegate = PlayerViewController.instance
        delegate?.trackDidChange(track)
    }
}

class PlayerViewHandler {
	
    var delegate: PlayerHandlerDelegate?
    
    func notify() {
        delegate = CanContainMiniPlayerViewController()
        delegate?.miniPlayerDidClose()
    }

}
